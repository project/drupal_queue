
DRUPAL QUEUE
------------

The queue system allows placing items in a queue and processing them later. The
system tries to ensure that only one consumer can process an item. This module
is an API meant to be used and depended upon by other modules and does not
expose any user interface to configure.

This is a direct backport of the Queue API in Drupal 7 core.


INSTALLATION
------------

- Install module
- Schedule workers: To provide full functionality without requiring additional
  cron configuration, jobs added to the queue are being worked off in subsequent
  cron runs. If this is not sufficient, it is recommended to add a separate
  process for workers: If using drush, add "drush queue-cron" to your
  crontab. Otherwise copy drupal_queue_cron.php to your site's root directory
  and add it to your crontab just like cron.php
- You can schedule as many workers concurrently as your server resources allow
  for.
- Note: modules that use Drupal Queue may still require cron to be configured
  http://drupal.org/cron


USING DRUPAL QUEUE
------------------

If your module uses the Drupal Queue API, note that jobs being queued need to be
concurrency-safe. For an example look at Drupal 7 aggregator module or Drupal 6
Feeds module.

http://cvs.drupal.org/viewvc/drupal/drupal/modules/aggregator/
http://drupal.org/project/feeds


API
---

See the included in-code documentation in drupal_queue.inc or view it online at
http://api.drupal.org/api/drupal/modules--system--system.queue.inc/group/queue/7
